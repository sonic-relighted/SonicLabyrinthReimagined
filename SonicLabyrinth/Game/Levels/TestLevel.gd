extends Spatial

func _ready():
	get_tree().call_deferred("call_group", "GAME_LISTENER", "game_listener_on_loaded", {
		"zone": "99",
		"act": "99",
		"times": [60, 70, 80, 90, 100], # [S A B C D] E
		"time": 600,
#		"timer_countdown": true,
		"is_boss": false,
		"next_zone": 1,
		"next_act": 2,
		"character": load("res://Game/Characters/Sonic.tscn"),
		"hud_components": [
			load("res://Engine/Hud/Timer/Timer.tscn"),
			load("res://Engine/Hud/Rings/Rings.tscn"),
			load("res://Engine/Hud/Score/Score.tscn"),
			load("res://Engine/Hud/Lifes/Lifes.tscn"),
			load("res://Engine/Hud/Underwater/Underwater.tscn"),
			load("res://Game/Ui/Hud/Keys/Keys.tscn")
		]
	})
