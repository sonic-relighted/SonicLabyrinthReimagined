extends Spatial

func _ready():
	get_tree().call_deferred("call_group", "GAME_LISTENER", "game_listener_on_loaded", {
		"zone": "1",
		"act": "4",
		"times": [60, 70, 80, 90, 100], # [S A B C D]
		"time": 60,
		"timer_countdown": true,
		"is_boss": true,
		"next_zone": 2,
		"next_act": 1,
		"character": load("res://Game/Characters/Sonic.tscn"),
		"hud_components": [
			load("res://Engine/Hud/Timer/Timer.tscn"),
			load("res://Engine/Hud/Rings/Rings.tscn"),
			load("res://Engine/Hud/Score/Score.tscn"),
			load("res://Engine/Hud/Lifes/Lifes.tscn"),
			load("res://Game/Ui/Hud/Keys/Keys.tscn")
		]
	})
	MusicSystem.play("res://Game/Audio/ost/Sky/Sonic_Resistance_Tutorial_20200802.ogg")
	$Sky.mesh.surface_get_material(0).set_shader_param("color", Color.black)
	$Sky.mesh.surface_get_material(0).set_shader_param("light_color", Color(.1, .1, .2))
	$GoalCapsule.global_translate(Vector3.DOWN * 20.0)
