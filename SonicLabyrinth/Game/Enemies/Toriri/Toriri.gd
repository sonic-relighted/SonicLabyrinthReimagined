extends Spatial

signal destroyed

export(float) var fly_length := 8.0
export(float) var fly_time := 2.0

onready var spawn_particles := $SpawnParticles
onready var model := $Model
onready var damage_area := $DamageArea
onready var tween := $Tween
onready var postexplosion_timer := $PostExplosionTimer
onready var respawn_timer := $RespawnTimer

onready var movement         = Vector2(sin(rotation.y), cos(rotation.y))
onready var default_position = global_transform.origin
onready var destination = default_position + Vector3(movement.x, .0, movement.y) * fly_length

func self_destroy(player):
	emit_signal("destroyed")

	if player:
		var impact = movement * 4.0 * fly_length / fly_time
		player.velocity.x += impact.x
		player.velocity.z += impact.y

	model.hide()
	damage_area.set_collision_mask_bit(6, false)
	tween.stop_all()
	VfxEmitter.explosion(global_transform.origin)
	global_transform.origin = default_position
	respawn_timer.stop()
	postexplosion_timer.start()

func _respawn():
	global_transform.origin = default_position
	tween.resume_all()
	tween.interpolate_property(self, "global_transform:origin",
			default_position, destination, fly_time,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()

func _on_PostExplosionTimer_timeout():
	spawn_particles.emitting = true
	respawn_timer.start()

func _on_RespawnTimer_timeout():
	model.show()
	damage_area.set_collision_mask_bit(6, true)
	spawn_particles.emitting = false
	_respawn()

func _on_Tween_tween_all_completed():
	call_deferred("self_destroy", null)

func _ready():
	model.hide()
	damage_area.set_collision_mask_bit(6, false)
