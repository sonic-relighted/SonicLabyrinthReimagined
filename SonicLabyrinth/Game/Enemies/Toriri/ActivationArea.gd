extends Area

func activate(activator):
	get_parent().respawn_timer.start()

func deactivate(activator):
	var host = get_parent()
	host.respawn_timer.stop()
	host.postexplosion_timer.stop()
	host.tween.stop_all()
	host.model.hide()
	host.damage_area.set_collision_mask_bit(6, false)

func _ready():
	call_deferred("deactivate", null)
