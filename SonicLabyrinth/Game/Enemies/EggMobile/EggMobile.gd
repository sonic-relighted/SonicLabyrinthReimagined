extends Spatial

signal destroyed

export(NodePath) var goal_capsule_nodepath : NodePath
export(float) var start_height : float = 8.0
export(float) var top_height   : float = 2.3
export(float) var end_height   : float = 15.0
export(float) var floor_height : float = -.50
export(float) var enter_speed  : float = 3.0
export(float) var turn_speed   : float = 5.0
export(float) var player_impulse : float = 3.0
export(float) var lifes        : int = 8

onready var space_state     : PhysicsDirectSpaceState = get_world().direct_space_state
onready var statemachine    := $Statemachine
onready var model           := $Model
onready var damage_area     := $DamageArea
onready var spikes_area     := $Spikes
onready var floor_particles := $Model/FloorParticles

onready var default_position := global_transform.origin

onready var destroyed := false

func player_listener_on_prepared(plyr):
	statemachine.init()
	statemachine.walk_state.player = plyr
	set_physics_process(true)

func self_destroy(plyr):
	model.damage()
	lifes -= 1
	if lifes <= 0:
		destroyed = true
		get_tree().call_group("BOSS_LISTENER", "boss_listener_on_destroyed")
	get_tree().call_group("BOSS_LISTENER", "boss_listener_on_life_updated", lifes)

func _physics_process(delta : float):
	statemachine.step(delta)

func _player_bounce(plyr):
	var diff : Vector3 = plyr.global_transform.origin - global_transform.origin
	diff.y = .0
	plyr.velocity += diff.normalized() * player_impulse

func _on_DamageArea_damage_has_collided(enmy, plyr):
	_player_bounce(plyr)

func _on_DamageArea_has_collided(enmy, plyr):
	_player_bounce(plyr)

func _on_Spikes_has_collided(enmy, plyr):
	_player_bounce(plyr)

func _on_Spikes_damage_has_collided(enmy, plyr):
	_player_bounce(plyr)

func _ready():
	set_physics_process(false)
	$DamageArea.connect("has_collided", self, "_on_DamageArea_has_collided")
	$DamageArea.connect("damage_has_collided", self, "_on_DamageArea_damage_has_collided")
	$Spikes.connect("has_collided", self, "_on_Spikes_has_collided")
	$Spikes.connect("damage_has_collided", self, "_on_Spikes_damage_has_collided")

	get_tree().call_deferred("call_group",
			"BOSS_LISTENER", "boss_listener_on_prepared", self)

