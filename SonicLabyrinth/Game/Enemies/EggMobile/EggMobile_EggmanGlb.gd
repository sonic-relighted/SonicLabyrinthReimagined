extends Spatial

onready var damage_hilight_material : ShaderMaterial = preload("res://Engine/BaiscMaterial/BossHilight.material")
onready var damage_timer : Timer = $DamageTimer

func damage():
	damage_hilight_material.set_shader_param("blink", true)
	damage_timer.start()

func walk():
	pass

func _on_DamageTimer_timeout():
	damage_hilight_material.set_shader_param("blink", false)
