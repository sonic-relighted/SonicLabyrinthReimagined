extends Node

onready var statemachine := get_parent()
onready var host := statemachine.get_parent()

var height : float

var timer : Timer

func enter():
	height = host.global_transform.origin.y
	_positioning()
	host.damage_area.queue_free()
	timer.start()
	_on_timeout()

func exit():
	pass

func step(delta : float):
	if height > host.end_height:  return statemachine.end_state
	_positioning()
	height += delta * host.enter_speed
	return self

func _positioning():
	host.global_transform.origin.y = height

func _on_timeout():
	var offset = Vector3.RIGHT.rotated(Vector3.UP, rand_range(-PI, PI))
	VfxEmitter.explosion(host.global_transform.origin + offset)

func _ready():
	timer = Timer.new()
	add_child(timer)
	timer.one_shot = false
	timer.wait_time = .4
	timer.connect("timeout", self, "_on_timeout")
