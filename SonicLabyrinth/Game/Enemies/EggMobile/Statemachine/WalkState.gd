extends Node

export(float) var speed_timer_increment := .5
export(float) var speed_timer_delay := 1.0

onready var speed_timer : Timer = $SpeedTimer

onready var statemachine := get_parent()
onready var host := statemachine.get_parent()

var player : Spatial

var destination : Vector2
var position2D  : Vector2
var default_position2D : Vector2
var movement_to_player : Vector2

var speed_timer_value : float
var height : float

func enter():
	height = host.start_height
	default_position2D = Vector2(host.default_position.x, host.default_position.z)
	_update_position()
	_update_destination(1.0)
	speed_timer.start(speed_timer_delay)
	speed_timer_value = .0

func exit():
	speed_timer.stop()

func step(delta : float):
	_update_position()
	_update_destination(delta)
	_update_movement_to_player()
	
	if host.destroyed:
		return statemachine.exit_state
	if movement_to_player.length() < .1:
		return statemachine.pre_down_state
	_move(delta)
	_turn(delta)
	return self

func _update_position():
	position2D = Vector2(host.global_transform.origin.x, host.global_transform.origin.z)

func _update_destination(delta : float):
	destination.x = player.global_transform.origin.x#lerp(destination.x, player.global_transform.origin.x, delta)
	destination.y = player.global_transform.origin.z#lerp(destination.y, player.global_transform.origin.z, delta)

func _update_movement_to_player():
	movement_to_player = destination - position2D

func _move(delta : float):
	var movement = movement_to_player.normalized()
	var ds = delta * speed_timer_value
	position2D.x = lerp(position2D.x, position2D.x + movement.x, ds)
	position2D.y = lerp(position2D.y, position2D.y + movement.y, ds)
	host.global_transform.origin = Vector3(
			position2D.x,
			host.global_transform.origin.y,
			position2D.y)

func _turn(delta : float):
	host.model.rotation.y = lerp_angle(
			host.model.rotation.y,
			-movement_to_player.angle() + PI,
			delta * host.turn_speed)

func _on_SpeedTimer_timeout():
	speed_timer_value += speed_timer_increment
