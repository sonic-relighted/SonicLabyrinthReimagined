extends Node

onready var statemachine := get_parent()
onready var host := statemachine.get_parent()

var tween : Tween

func enter():
	var capsule = host.get_node(host.goal_capsule_nodepath)
	var pos = capsule.global_transform.origin
	pos.y = host.global_transform.origin.y
	var result = host.space_state.intersect_ray(
			pos,
			pos + Vector3.DOWN * 100.0,
			[], 1024)
	tween.interpolate_property(capsule,
			"global_transform:origin",
			pos, result.position, 1.0,
			Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	tween.start()

func exit():
	pass

func step(delta : float):
	return self

func _create_tween():
	tween = Tween.new()
	add_child(tween)

func _ready():
	_create_tween()
