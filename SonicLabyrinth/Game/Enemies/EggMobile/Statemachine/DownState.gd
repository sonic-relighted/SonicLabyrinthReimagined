extends Node

onready var statemachine := get_parent()
onready var host := statemachine.get_parent()

var tween : Tween
var timer : Timer

var waiting : bool

func enter():
	waiting = true
	tween.interpolate_property(host, "global_transform:origin:y",
			host.top_height, host.floor_height, .5,
			Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	tween.start()
	timer.start()

func exit():
	pass

func step(delta : float):
	if host.destroyed:
		return statemachine.exit_state
	if not waiting:
		return statemachine.pre_up_state
	return self

func _on_tween_completed():
	waiting = false

func _create_tween():
	tween = Tween.new()
	add_child(tween)
	tween.connect("tween_all_completed", self, "_on_tween_completed")

func _on_timer_timeout():
	host.floor_particles.emitting = true

func _create_timer():
	timer = Timer.new()
	add_child(timer)
	timer.one_shot = true
	timer.connect("timeout", self, "_on_timer_timeout")
	timer.wait_time = .2

func _ready():
	_create_tween()
	_create_timer()
