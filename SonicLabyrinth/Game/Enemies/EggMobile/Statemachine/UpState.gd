extends Node

onready var statemachine := get_parent()
onready var host := statemachine.get_parent()

var tween : Tween

var waiting : bool

func enter():
	waiting = true
	tween.interpolate_property(host, "global_transform:origin:y",
			host.floor_height, host.top_height, 1.5,
			Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()

func exit():
	pass

func step(delta : float):
	if host.destroyed:
		return statemachine.exit_state
	if not waiting:
		return statemachine.walk_state
	return self

func _on_tween_completed():
	waiting = false

func _ready():
	tween = Tween.new()
	add_child(tween)
	tween.connect("tween_all_completed", self, "_on_tween_completed")
