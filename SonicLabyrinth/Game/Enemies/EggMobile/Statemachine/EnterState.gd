extends Node

onready var statemachine := get_parent()
onready var host := statemachine.get_parent()

var height : float

func enter():
	height = host.start_height
	_positioning()

func exit():
	pass

func step(delta : float):
	if host.destroyed: return statemachine.exit_state
	if height < host.top_height: return statemachine.walk_state
	_positioning()
	height -= delta * host.enter_speed
	return self

func _positioning():
	host.global_transform.origin.y = height
