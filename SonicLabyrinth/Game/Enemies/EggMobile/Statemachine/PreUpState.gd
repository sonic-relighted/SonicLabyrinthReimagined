extends Node

onready var statemachine := get_parent()
onready var host := statemachine.get_parent()

var tween : Tween

var waiting : bool

func enter():
	waiting = true
	tween.interpolate_property(host.model, "scale:y",
			host.model.scale.y * 1.1, host.model.scale.y, 1.0,
			Tween.TRANS_BOUNCE, Tween.EASE_IN)
	tween.start()

func exit():
	host.floor_particles.emitting = false

func step(delta : float):
	if host.destroyed:
		return statemachine.exit_state
	if not waiting:
		return statemachine.up_state
	return self

func _on_tween_completed():
	waiting = false

func _create_tween():
	tween = Tween.new()
	add_child(tween)
	tween.connect("tween_all_completed", self, "_on_tween_completed")

func _ready():
	_create_tween()
