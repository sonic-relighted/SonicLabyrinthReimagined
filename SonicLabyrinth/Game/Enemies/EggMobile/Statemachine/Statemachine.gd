extends Node

onready var enter_state    := $EnterState
onready var walk_state     := $WalkState
onready var pre_down_state := $PreDownState
onready var down_state     := $DownState
onready var pre_up_state   := $PreUpState
onready var up_state       := $UpState
onready var exit_state     := $ExitState
onready var end_state      := $EndState

onready var current_state := enter_state
onready var previous_state := current_state

func force(new_state):
	previous_state = current_state
	current_state = new_state
	previous_state.exit()
	current_state.enter()

func step(delta : float):
	var next = current_state.step(delta)
	if next != current_state:
		force(next)

func init():
	current_state.enter()
