extends Spatial

onready var playback : AnimationNodeStateMachinePlayback = $AnimationTree.get("parameters/playback")

func idle():
	playback.travel("Idle")

func walk():
	playback.travel("Run")

func hit():
	playback.travel("Hit")
