extends KinematicBody

signal destroyed

export(float) var max_distance := 4.0
export(float) var rot_speed := 3.0
export(float) var idle_time := 3.0
export(float) var walk_time := 2.0
export(float) var walk_attack_time := .5
export(float) var fire_attack_time  := .5
export(float) var hit_distance := .7
export(float) var gravity := 3.0


export(PackedScene) var projectile_template : PackedScene

enum State {
	UNDEFINED,
	IDLE,
	WALK,
	WALK_ATTACK,
	FIRE_ATTACK
}

onready var model := $Model
onready var timer := $StateTimer
onready var attack_area := $AttackArea
onready var damage_area := $DamageArea
onready var projectile_output = $ProjectileOutput

onready var state : int = State.UNDEFINED
onready var default_position := global_transform.origin
onready var velocity : Vector3 = Vector3.ZERO

var destination : Vector2

const ROT_OFFSET := PI * 1.5

func activate():
	set_physics_process(true)
	_on_StateTimer_timeout()

func deactivate():
	set_physics_process(false)
	state = State.UNDEFINED
	timer.stop()

func start_idle():
	state = State.IDLE
	timer.start(idle_time)
	model.idle()

func start_walk():
	state = State.WALK
	timer.start(walk_time)
	model.walk()
	_choose_destination()

func start_attack():
	state = State.WALK_ATTACK
	timer.start(walk_attack_time)
	model.walk()
	_choose_destination()

func start_fire_attack():
	state = State.FIRE_ATTACK
	timer.start(fire_attack_time)
	model.hit()
	_instance_projectile()

func self_destroy(player):
	VfxEmitter.explosion(global_transform.origin + Vector3.UP)
	damage_area.set_collision_mask_bit(6, false)
	emit_signal("destroyed")
	queue_free()

func _physics_process(delta : float):
	match state:
		State.UNDEFINED: _undefined(delta)
		State.IDLE: _idle(delta)
		State.WALK: _walk(delta)
		State.WALK_ATTACK: _walk_attack(delta)
		State.FIRE_ATTACK: _fire_attack(delta)

func _undefined(delta : float):
	_on_StateTimer_timeout()

func _idle(delta : float):
	_move(delta, .0)

func _walk(delta : float):
	var diff = Vector2(
			global_transform.origin.x,
			global_transform.origin.z)-destination
	rotation.y = lerp_angle(
			rotation.y,
			-diff.angle() + ROT_OFFSET,
			delta * rot_speed)
	_move(delta, 1.0)

func _walk_attack(delta : float):
	var diff = Vector2(
			global_transform.origin.x,
			global_transform.origin.z) - Vector2(
					attack_area.target.global_transform.origin.x,
					attack_area.target.global_transform.origin.z
			)
	if diff.length() < hit_distance:
		start_fire_attack()
	else:
		rotation.y = lerp_angle(
				rotation.y,
				-diff.angle() + ROT_OFFSET,
				delta * rot_speed)
	diff = Vector2(
			global_transform.origin.x,
			global_transform.origin.z)-destination
	_move(delta, 3.0, diff.normalized())

func _fire_attack(delta : float):
	var diff = Vector2(
			global_transform.origin.x,
			global_transform.origin.z) - Vector2(
					attack_area.target.global_transform.origin.x,
					attack_area.target.global_transform.origin.z
			)
	rotation.y = lerp_angle(
			rotation.y,
			-diff.angle() + ROT_OFFSET,
			delta * rot_speed)

func _choose_destination():
	var rand_angle = rand_range(-PI, PI)
	var point = Vector2(
			sin(rand_angle) * max_distance,
			cos(rand_angle) * max_distance)
	destination = Vector2(
			default_position.x + point.x,
			default_position.z + point.y)

func _move(delta : float, speed : float, movement = null):
	if movement:
		velocity.x = movement.x
		velocity.z = movement.y
	else:
		velocity.x = global_transform.basis.z.x * speed
		velocity.z = global_transform.basis.z.z * speed
	velocity.y -= gravity
	velocity = move_and_slide(velocity)

func _instance_projectile():
	var projectile = projectile_template.instance()
	get_parent().add_child(projectile)
	projectile.global_transform = projectile_output.global_transform

func _on_StateTimer_timeout():
	match state:
		State.UNDEFINED: start_walk()
		State.IDLE: start_walk()
		State.WALK: start_idle()
		State.WALK_ATTACK: start_fire_attack()
		State.FIRE_ATTACK: start_attack()
