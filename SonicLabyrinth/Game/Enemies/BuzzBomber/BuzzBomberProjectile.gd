extends Area

var speed := 5.0

func damage_collided(player):
	get_tree().call_group("PLAYER_LISTENER", "player_listener_on_player_damaged", player)
	_destroy()

func _physics_process(delta : float):
	global_transform.origin += global_transform.basis.x * delta * speed

func _on_BuzzBomberProjectile_body_entered(body):
	_destroy()

func _destroy():
	VfxEmitter.explosion(global_transform.origin)
	queue_free()
