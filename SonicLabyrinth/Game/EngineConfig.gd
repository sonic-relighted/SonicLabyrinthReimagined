extends Node

#
# All these variables are not constants because I think will be interesting
# allow to change  by code
#

# ==== GAMEPLAY CONFIG ==== #
# Loss al rings when you have damage = true
# If false, only 30 rings will be lost
onready var LOSS_ALL_RINGS_ON_DAMAGE : float = true
# Death if player damage wigh 0 rings
onready var DEATH_ON_DAMAGE_WITH_NO_RINGS : float = false

# ==== MENU CONFIG ==== #
# Background textures
onready var MENU_MUSIC_CONTROLLER                        : String = "res://Game/Ui/Menus/Controller/MenuMusicController.tscn"
onready var MENU_BACKGROUND_CONTROLLER                   : String = "res://Game/Ui/Menus/Controller/MenuBackgroundController.tscn"
onready var MENU_POINTER_SELECTED_IMAGE_FILE             : String = "res://Engine/MainScenes/StartMenu/Textures/OptionPointerOn.png"
onready var MENU_POINTER_UNSELECTED_IMAGE_FILE           : String = "res://Engine/MainScenes/StartMenu/Textures/OptionPointerOff.png"
onready var MENU_POINTER_SELECTED_MODULATE               : Color = Color.yellow

# Levels
onready var MENU_LEVEL_SELECTOR_ON_LEVEL_COMPLETED       : bool = false
onready var MENU_LEVEL_SELECTOR_RESET_RINGS_ON_OPEN      : bool = true
onready var MENU_LEVEL_SELECTOR_RESET_SHIELD_ON_OPEN     : bool = true
onready var MENU_LEVEL_SELECTOR_CONTENT_SCENE            : String = "res://Engine/MainScenes/LevelSelector/DefaultLevelSelectorContent.tscn"
onready var MENU_LEVEL_SELECTOR_LEVELS                   := [
	{ "zone_id": "01", "acts" : [ "01", "02", "03", "04" ] }
]
