extends Area

export(NodePath) var target : NodePath

onready var restore_tween := $RestoreTween

onready var catched := false
onready var restored := false
onready var default_parent : Node = get_parent()

const RAY_SNAP_START_POSITION := Vector3.UP * 1.0
const RAY_SNAP_END_POSITION := Vector3.DOWN * 2.0
const CATCHED_POSITION = Vector3.DOWN * 10.0

func activate(activator):
	if catched: return
	if activator is Player:
		_catch()

func deactivate(activator):
	pass

func restore(position : Vector3, position2 : Vector3):
	restored = true
	remove_from_group("KEY_CATCHED")
	global_transform.origin.y = position.y
	restore_tween.interpolate_property(self, "global_transform:origin:x",
			position.x, position2.x, .25, Tween.TRANS_SINE, Tween.EASE_OUT)
	restore_tween.interpolate_property(self, "global_transform:origin:z",
			position.z, position2.z, .25, Tween.TRANS_SINE, Tween.EASE_OUT)
	restore_tween.interpolate_method(self, "_snap_to_floor",
			.0, .0, .25)
	restore_tween.start()

func _catch():
	VfxEmitter.ring(global_transform.origin)
	catched = true
	get_tree().call_group("PLAYER_LISTENER", "player_listener_on_time_item_catched", restored == false)
	get_tree().call_group("PLAYER_LISTENER", "player_listener_on_key_catched", restored == false)
	global_transform.origin = CATCHED_POSITION
	add_to_group("KEY_CATCHED")

func _on_RestoreTween_tween_all_completed():
	catched = false

func _on_target_destroyed():
	var duplicated = duplicate()
	default_parent.add_child(duplicated)
	duplicated.default_parent = default_parent
	duplicated.global_transform.origin = global_transform.origin
	queue_free()

func _attack_to_target():
	set_collision_mask_bit(8, false)
	if target and target != "":
		yield(get_tree(), "idle_frame")
		var duplicated = duplicate()
		duplicated.target = ""
		get_node(target).add_child(duplicated)
		duplicated.default_parent = default_parent
		duplicated.global_transform.origin = global_transform.origin
		queue_free()
	else:
		yield(get_tree(), "idle_frame")
		if get_parent().is_in_group("ENEMY"):
			get_parent().connect("destroyed", self, "_on_target_destroyed")
		else:
			_snap_to_floor()
			set_collision_mask_bit(8, true)

func _process(delta : float):
	rotation.y += delta

func _snap_to_floor(not_used_param_1 = null, not_used_param_2 = null):
	var state = get_world().direct_space_state
	var ray_result = state.intersect_ray(
			global_transform.origin + RAY_SNAP_START_POSITION,
			global_transform.origin + RAY_SNAP_END_POSITION,
			[], 1024
			)
	if ray_result.size():
		global_transform.origin = ray_result.position

func _ready():
	_attack_to_target()
