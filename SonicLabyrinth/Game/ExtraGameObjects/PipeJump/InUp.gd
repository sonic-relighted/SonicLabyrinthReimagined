extends Area

export(NodePath) var destination : NodePath

func activate(activator):
	activator.teleport = get_node(destination).global_transform

func deactivate(activator):
	pass
