extends Area

signal player_entered

export(NodePath) var destination : NodePath

func activate(activator):
	activator.teleport = get_node(destination).global_transform
	emit_signal("player_entered")

func deactivate(activator):
	pass
