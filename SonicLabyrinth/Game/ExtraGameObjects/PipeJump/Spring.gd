extends Spatial

onready var tween = $Tween

onready var default_position := translation

func _on_InBottom_player_entered():
	tween.stop_all()
	tween.interpolate_property(self, "translation:y",
			default_position.y + 3.0, default_position.y, 1.0,
			Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	tween.start()
