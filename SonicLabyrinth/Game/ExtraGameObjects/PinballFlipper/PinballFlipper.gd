extends Spatial

export(float) var force = 40.0
export(float) var up_force = 5.0
export(bool) var front_trigger_enabled : bool = true
export(bool) var back_trigger_enabled : bool = true

onready var model   : Spatial = $Model
onready var front   : Area = $AreaFront
onready var back    : Area = $AreaBack
onready var tween   : Tween = $Tween

func hit(angle : float, activator):
	tween.stop_all()
	tween.resume_all()
	tween.interpolate_property(model,
			"rotation:y",
			angle, .0, 1.0,
			Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	tween.start()
	front.set_collision_mask_bit(7, false)
	back.set_collision_mask_bit(7, false)
	
	var diff_position = (activator.impulse_point.global_transform.origin - global_transform.origin)
	var diff_2d = Vector2(diff_position.x, diff_position.z)
	var leng = clamp(diff_2d.length(), .3, 10.0)
	var impulse : Vector2
	if angle < .0:
		impulse = Vector2.DOWN\
				.rotated(-rotation.y - leng * 1.5 + 1.2)\
				* force * leng * leng
	else:
		impulse = Vector2.UP \
				.rotated(-rotation.y + leng * 1.5 - 1.2)\
				* force * leng * leng

	activator.velocity.x = impulse.x
	activator.velocity.z = impulse.y
	activator.velocity.y = up_force

func _reset_colliders():
	front.set_collision_mask_bit(7, front_trigger_enabled)
	back.set_collision_mask_bit(7, back_trigger_enabled)

func _on_Tween_tween_all_completed():
	_reset_colliders()

func _ready():
	_reset_colliders()
