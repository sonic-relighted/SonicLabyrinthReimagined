extends Area

export(float) var angle := -PI * .25

func collided(activator):
	get_parent().hit(angle, activator)
