extends ColorRect

onready var tween := $Tween

func player_listener_on_shield_lost(plyr : SonicPlayer):
	if plyr.shield_manager.shield_type == Constants.RelightedEngineShieldTypes.ELECTRIC \
	or plyr.shield_manager.shield_type == Constants.RelightedEngineShieldTypes.FIRE:
		flash()

func flash():
	tween.stop_all()
	tween.reset_all()
	tween.interpolate_property(self, "modulate:a",
			1.0, .0, .25,
			Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.start()

func _enter_tree():
	modulate.a = .0
