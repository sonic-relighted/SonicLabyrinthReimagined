extends HBoxContainer

onready var keys_collected := 0
onready var shielded := false
onready var invincible := false

func player_listener_on_monitor_destroyed(monitor : Monitor):
	match monitor.type:
		Monitor.MonitorType.INVINCIBLE: invincible = true
		Monitor.MonitorType.BASIC_SHIELD: shielded = true
		Monitor.MonitorType.SHIELD_1: shielded = true
		Monitor.MonitorType.SHIELD_2: shielded = true
		Monitor.MonitorType.SHIELD_3: shielded = true
		Monitor.MonitorType.SHIELD_4: shielded = true

func player_listener_on_shield_lost(plyr):
	shielded = false

func player_listener_on_key_catched(first_time : bool):
	increment(1)

func player_listener_on_player_damaged(plyr):
	if shielded or invincible: return
	reset()

func increment(n : int):
	keys_collected += n
	_update_keys()

func reset():
	keys_collected = 0
	_update_keys()

func _update_keys():
	for child in get_children():
		child.visible = child.get_index() < keys_collected

func _ready():
	_update_keys()
