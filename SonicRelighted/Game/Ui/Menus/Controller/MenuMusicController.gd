extends Node

func menu_listener_on_new_game_created(slot_id : int):
	MusicSystem.stop(.5)

func menu_listener_on_title_opened():
	MusicSystem.stop(.5)

func menu_listener_on_principal_opened():
	MusicSystem.play("res://Game/Audio/ost/Menu/menu_a.ogg", 4.0)

func menu_listener_on_slots_opened():
	MusicSystem.play("res://Game/Audio/ost/Menu/menu_b.ogg", 4.0)

func menu_listener_on_options_opened():
	pass

func menu_listener_on_controls_opened():
	pass

func menu_listener_on_credits_opened():
	pass

func menu_listener_on_settings_popup_closed(result):
	pass

func menu_listener_on_controls_closed():
	pass

func menu_listener_on_credits_closed():
	pass
