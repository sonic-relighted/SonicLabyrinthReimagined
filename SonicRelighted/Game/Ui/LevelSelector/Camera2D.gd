extends Camera2D

export(NodePath) var target_np : NodePath

onready var target = get_node(target_np)

func teleport(new_road_destination : Node2D):
	global_position = new_road_destination.global_position

func _physics_process(delta : float):
	global_position = global_position.linear_interpolate(target.global_position, .1)
