extends Node2D

signal level_opened(id)
signal back

func show_levels():
	print("sin implementar")

func _on_Pointer_level_opened(id):
	emit_signal("level_opened", id)

func _on_Pointer_back():
	emit_signal("back")

func _exit_tree():
	get_tree().set_screen_stretch(
			SceneTree.STRETCH_MODE_DISABLED,
			SceneTree.STRETCH_ASPECT_IGNORE,
			Vector2(1280.0, 720.0))

func _ready():
	get_tree().set_screen_stretch(
			SceneTree.STRETCH_MODE_2D,
			SceneTree.STRETCH_ASPECT_KEEP_WIDTH,
			Vector2(640.0, 360.0))
	
	$Pointer.teleport($Map/RoadNode)
	$Camera2D.teleport($Pointer)
