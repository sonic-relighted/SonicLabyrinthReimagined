extends Node2D

signal level_opened(id)
signal back

export(float) var speed : float = 128.0
export(NodePath) var navigation_np : NodePath

onready var navigation : Navigation2D = get_node(navigation_np)

onready var path_manager := $PathManager
onready var rotation_manager := $RotationManager

onready var current_selection : Node2D = null

func teleport(new_road_destination : Node2D):
	current_selection = new_road_destination
	global_position = current_selection.global_position

func _move(delta : float):
	var direction = path_manager.get_direction()
	if direction == null: return
	global_position += direction * speed * delta

func _on_PathManager_intermediate_arrived(pos : Vector2, next_pos : Vector2):
	global_position = pos
	if pos == next_pos: return
	rotation_manager.change_destination(next_pos)

func _on_PathManager_arrived(new_road_node):
	current_selection = new_road_node
	set_physics_process(false)
	rotation_manager.stop()

func _choose_direction(road_node : Node2D):
	if road_node == null: return
	path_manager.change_destination(road_node)
	current_selection = null
	set_physics_process(true)

func _open_level():
	var level_id = current_selection.get_level_id()
	if level_id == "": return
	emit_signal("level_opened", level_id)
	set_process(false)

func _back():
	emit_signal("back")
	set_process(false)

func _physics_process(delta : float):
	_move(delta)

func _process(delta : float):
	if current_selection == null: return

	if Input.is_action_just_pressed("ui_up"):
		_choose_direction(current_selection.get_above())
	if Input.is_action_just_pressed("ui_down"):
		_choose_direction(current_selection.get_below())
	if Input.is_action_just_pressed("ui_left"):
		_choose_direction(current_selection.get_left())
	if Input.is_action_just_pressed("ui_right"):
		_choose_direction(current_selection.get_right())
	if Input.is_action_just_pressed("ui_accept"):
		_open_level()
	if Input.is_action_just_pressed("ui_cancel"):
		_back()
	
func _ready():
	set_physics_process(false)
