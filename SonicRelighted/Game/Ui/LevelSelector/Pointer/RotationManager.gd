extends Node

export(float) var speed : float = 3.0

onready var host := get_parent()

onready var origin      : Vector2 = Vector2.ZERO
onready var destination : Vector2 = Vector2.ZERO

func change_destination(new_destination : Vector2):
	origin = host.global_position
	destination = new_destination
	set_physics_process(true)

func stop():
	set_physics_process(false)

func _physics_process(delta : float):
	var diff = origin - destination
	var next_angle = diff.angle()
	if abs(next_angle - host.rotation) < .01:
		host.rotation = next_angle
		set_physics_process(false)
		return
	host.rotation = lerp_angle(host.rotation, next_angle, speed * delta)

func _ready():
	set_physics_process(false)
