extends Node

signal intermediate_arrived
signal arrived

onready var host := get_parent()

onready var road_node_destination : Node2D = null
onready var path : Array = []

func change_destination(new_road_node : Node2D):
	road_node_destination = new_road_node
	path = host.navigation.get_simple_path(host.global_position, road_node_destination.global_position)
#	path.remove(0)

func get_direction():
	if path.size() == 0:
		emit_signal("arrived", road_node_destination)
		road_node_destination = null
		return null

	var diff : Vector2 = path[0] - host.global_position
	if diff.length() <= 4.0:
		emit_signal("intermediate_arrived", path[0], path[1] if path.size() > 1 else path[0])
		path.remove(0)
		return get_direction()
	
	return diff.normalized()

func has_destination():
	return road_node_destination != null
