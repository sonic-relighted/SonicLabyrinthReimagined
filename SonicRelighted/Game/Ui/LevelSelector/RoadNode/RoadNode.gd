extends Position2D

export(String) var level_id = ""

export(NodePath) var above : NodePath
export(NodePath) var below : NodePath
export(NodePath) var left  : NodePath
export(NodePath) var right : NodePath

func get_above():
	if above == null or above == "": return null
	return get_node(above)

func get_below():
	if below == null or below == "": return null
	return get_node(below)

func get_left():
	if left == null or left == "": return null
	return get_node(left)

func get_right():
	if right == null or right == "": return null
	return get_node(right)

func get_level_id() -> String:
	return level_id
